<?php

namespace L4p1n\Laravel;

use Illuminate\Support\ServiceProvider;
use L4p1n\Cache;
use L4p1n\Drivers\RedisDriver;

class CacheServiceProvider extends ServiceProvider{
	public function register(){
		$this->app->singleton(Cache::class, function($app){
			return new Cache(new RedisDriver());
		});
	}
}