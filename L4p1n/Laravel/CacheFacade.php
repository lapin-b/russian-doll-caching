<?php

namespace L4p1n\Laravel;

use \Illuminate\Support\Facades\Facade;
use L4p1n\Cache;

class CacheFacade extends Facade{
	protected static function getFacadeAccessor(){
		return Cache::class;
	}
}