<?php
namespace L4p1n;


use L4p1n\Interfaces\CacheInterface;

class Cache{
	/**
	 * @var CacheInterface
	 */
	private $driver;

	public function __construct(CacheInterface $driver){
		$this->driver = $driver;
	}

	public function cache($keys, callable $callback){
		$key = $this->hashKeys($keys);
		$key = 'L4p1nRC:' . $key; // Create folder

		$value = $this->driver->get($key);
		if($value){
			echo $value;
			return;
		}

		ob_start();
		$callback();
		$value = ob_get_clean();

		$this->driver->set($key, $value);
		echo $value;
	}

	private function hashKeys($keys){
		if(is_array($keys)){
			$key = [];

			foreach($keys as $k){
				array_push($key, $this->hashKey($k));
			}

			return implode('_', $key);
		}

		return $this->hashKey($keys);
	}

	private function hashKey($key){
		if(is_a($key, 'Illuminate\Database\Eloquent\Model')){
			$key = str_replace('\\', '_', get_class($key) . '_' . $key->id . '_' . $key->created_at->timestamp);

			return $key;
		}

		return $key;
	}
}