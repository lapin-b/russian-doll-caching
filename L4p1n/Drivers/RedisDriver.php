<?php
namespace L4p1n\Drivers;

use L4p1n\Interfaces\CacheInterface;
use Predis\Client;

class RedisDriver implements CacheInterface{

	private $client;

	public function __construct(){
		$this->client = new Client();
	}

	/**
	 * Retrieves a key in the cache
	 *
	 * @param $key
	 * @return mixed
	 */
	public function get($key){
		return $this->client->get($key);
	}

	/**
	 * Sets a key in the cache
	 *
	 * @param $key
	 * @param $value
	 * @return mixed
	 */
	public function set($key, $value){
		$this->client->set($key, $value);
	}
}