<?php

namespace L4p1n\Interfaces;


/**
 * Interface CacheInterface
 * @package L4p1n\Interfaces
 */
interface CacheInterface{
	/**
	 * Retrieves a key in the cache
	 *
	 * @param $key
	 * @return mixed
	 */
	public function get($key);

	/**
	 * Sets a key in the cache
	 *
	 * @param $key
	 * @param $value
	 * @return mixed
	 */
	public function set($key, $value);
}